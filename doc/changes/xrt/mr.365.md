---
- mr.367
---
Improve Doxygen documentation of interfaces. Now the inheritance structure and implementation of interfaces is shown in the docs, and helper functions that call through function pointers are listed as "member functions", to help developers understand the internal structure of Monado better.
