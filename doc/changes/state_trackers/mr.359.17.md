OpenXR: Rework the logging formatting of error messages, this makes it easier to
read for the application developer.
